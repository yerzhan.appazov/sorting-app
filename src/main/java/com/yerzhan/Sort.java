package com.yerzhan;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Sort {
    public static final Logger logger = LogManager.getLogger(Sort.class);

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = (sc.nextInt());
        }
        sort(array);
        for (int num : array) {
            System.out.print(num + " ");
        }
    }

    public static void sort(int[] array){
        if (array == null) {
            logger.error("The received array is pointing to null!");
            throw new IllegalArgumentException();
        }
        int n = array.length;
        if (n > 10) {
            logger.error("The number of inputs exceeded 10");
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        logger.info("The sort function is finished!");
    }
}
