package com.yerzhan;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertArrayEquals;

public class SortTest {
    Sort sorting = new Sort();

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidCornerCase() {
        int[] array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        sorting.sort(array);
    }

    @Test
    public void testCornerCase0() {
        int[] array = {};
        sorting.sort(array);
        assertEquals(0, array.length);
    }

    @Test
    public void testCornerCase1() {
        int[] array = {1};
        sorting.sort(array);
        assertEquals(1, array[0]);
    }

    @Test
    public void testCornerCase10() {
        int[] array = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        int[] expectedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        sorting.sort(array);
        assertArrayEquals(expectedArray, array);
    }
    @Test
    public void testSortedArraysCase() {
        int[] array = {1, 2, 4, 8, 16, 32, 64};
        int[] expectedArray = {1, 2, 4, 8, 16, 32, 64};
        sorting.sort(array);
        assertArrayEquals(expectedArray, array);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullCase() {
        sorting.sort(null);
    }


}
