package com.yerzhan;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class ParametrizedSortTest {
    protected Sort sorting = new Sort();

    private int[] inputted;
    private int[] expected;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{4, 7, 3, 6, 1, 4}, new int[]{1, 3, 4, 4, 6, 7}},
                {new int[]{9, 7, 5, 3, 1, 0}, new int[]{0, 1, 3, 5, 7, 9}},
                {new int[]{0, 0, 0, 0, 0, 0}, new int[]{0, 0, 0, 0, 0, 0}},
                {new int[]{345, 445, 123, 920, 563, 700}, new int[]{123, 345, 445, 563, 700, 920}}
        });
    }

    public ParametrizedSortTest(int[] inputted, int[] expected) {
        this.inputted = inputted;
        this.expected = expected;
    }

    @Test
    public void sortTestingWithParameters() {
        sorting.sort(inputted);
        assertArrayEquals(expected, inputted);
    }
}
