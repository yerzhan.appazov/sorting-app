package com.yerzhan;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)

public class ParametrizedSortTestUsingCSV {
    protected Sort sorting = new Sort();

    private String inputted;
    private String expected;

    @Parameterized.Parameters
    public static Collection testData() throws IOException {
        return getTestData("src\\test\\resources\\parameters.csv");
    }

    public ParametrizedSortTestUsingCSV(String inputted, String expected) {
        this.inputted = inputted;
        this.expected = expected;
    }

    @Test
    public void sortTestingWithParametersInCSV() {
        int[] array = stringToIntArray(inputted);
        sorting.sort(array);
        assertEquals(expected, arrayToString(array));
    }

    public static Collection<String[]> getTestData(String fileName)
            throws IOException {
        List<String[]> records = new ArrayList<>();
        String record;
        BufferedReader file = new BufferedReader(new FileReader(fileName));
        while ((record = file.readLine()) != null) {
            String fields[] = record.split(":");
            records.add(fields);
        }
        file.close();
        return records;
    }

    public static int[] stringToIntArray(String str) {
        String[] strArray = str.split(",");
        int[] intArray = new int[strArray.length];
        for (int i = 0; i < strArray.length; i++) {
            intArray[i] = Integer.parseInt(strArray[i]);
        }
        return intArray;
    }

    public static String arrayToString(int[] array) {
        String sortedResult = "";
        for (int i = 0; i < array.length; i++) {
            if (i == 0) {
                sortedResult = String.valueOf(array[i]);
            } else
                sortedResult += "," + array[i];
        }
        return sortedResult;
    }
}
